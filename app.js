$(function() {
    // Elements
    const $meta = $("#meta");
    const $user = $("#user");
    const $loaded = $("#load-date");
    const $form = $("#time_keeper");
    const $formSubmit = $("#button-submit");

    const $alertSuccess = $("#alert-success");
    const $alertError = $("#alert-error");
    const $loginPrompt = $("#login-prompt");

    // Kimai variables
    let currentUserID;
    let currentUserName;
    const kimaiProcessorUrl = "/extensions/ki_timesheets/processor.php";
    const kimaiDateFormat =
        "[start_day=]DD[%2F]MM[%2F]YYYY[&end_day=]DD[%2F]MM[%2F]YYYY";

    // Get the userID and user name from Kimai cookies
    $.each(document.cookie.split(/; */), function() {
        if (this.includes("ki_active_tab_path_")) {
            let idCookieSplit = this.split("=");
            let idSubstring = idCookieSplit[0].substring(19);

            currentUserID = idSubstring;
        }
        if (this.includes("kimai_user")) {
            let nameCookieSplit = this.split("=");
            let nameValue = nameCookieSplit[1];

            if (nameValue !== "0") {
                currentUserName = nameValue;
            }
        }
    });

    // If we have a user name/ID show form, if not, show a login prompt
    if (currentUserID && currentUserName) {
        $form.removeClass("d-none");
        $meta.removeClass("d-none");
        $user.text(`${currentUserName}`);
        document.title = `${currentUserName} - Kimai Minimal`;
    } else {
        $loginPrompt.removeClass("d-none");
    }

    // Load kimai in modal to login
    $('#login-modal').on('shown.bs.modal', function () {
        $(this)
            .find("iframe")
            .attr("src", "https://kimai.tsgapis.com/");

        $(this).find("#done-reload").on('click', function() {
            location.reload();
        });
    })

    // Staleness Checker
    const niceDateFormat = "dddd Do MMMM";
    const dayOnLoad = moment().format(niceDateFormat);
    let currentDay;
    // The reload if stale function
    function reloadOnStale() {
        currentDay = moment().format(niceDateFormat);
        if (dayOnLoad !== currentDay) {
            location.reload();
        }
    }
    // Run every 2 mins
    setInterval(reloadOnStale, 300000);
    // Display Date in page
    $loaded.text(dayOnLoad);

    // Then the project, date and times
    let dateList = [];

    let today = moment().format();
    let yesterday = moment()
        .subtract(1, "days")
        .format();
    // Number of previous days to show
    let goBackBy = 10;
    // Days to exclude (weekends)
    const sunday = moment.weekdays()[0];
    const saturday = moment.weekdays()[6];
    // Loop through the days and create text and value for each in the array
    while (goBackBy >= 0) {
        let day = moment().subtract(goBackBy, "days");
        let dayAsName = day.format("dddd");

        if (dayAsName !== saturday && dayAsName !== sunday) {
            if (day.format() === today) {
                dateList.push({
                    value: day.format(kimaiDateFormat),
                    text: "Today"
                });
            } else if (day.format() === yesterday) {
                dateList.push({
                    value: day.format(kimaiDateFormat),
                    text: "Yesterday"
                });
            } else {
                dateList.push({
                    value: day.format(kimaiDateFormat),
                    text: day.format("dddd Do MMMM")
                });
            }
        }
        goBackBy--;
    }
    // Re-order the array
    let dateListOrdered = dateList.reverse();
    // Push the dates into the array in the new order
    $.each(dateList, function(i, item) {
        $("#date").append(
            $("<option>", {
                value: item.value,
                text: item.text
            })
        );
    });

    // Set up the entry string for Kimai ajax call
    function getKimaiString(user) {
        let entryProject = $("#projectID").val();
        let entryActivity = $("#activityID").val();
        let entryDate = $("#date").val();
        let entryTime = $("input[name=timeframe]:checked").val();

        let string = `id=&axAction=add_edit_timeSheetEntry&${entryProject}&filter=&${entryActivity}&filter=&description=&${entryDate}&${entryTime}&duration=&location=&trackingNumber=&comment=&commentType=0&userID%5B%5D=${user}&budget=&approved=&statusID=1&billable=0`;

        return string;
    }

    // Debug code to preview the userID and ajax string
    //
    // console.log("Current userID is " + currentUserID);
    // console.log('Initial Kimai string is - ' + getKimaiString(currentUserID));
    // $('input, select').on('change', function() {
    //     console.log('New Kimai string is - ' + getKimaiString(currentUserID));
    // });

    // Ensure the submit button is enabled when a field is changed.
    // The button is enabled initially incase the current fields are correct
    $("input, select").on("change", function() {
        $formSubmit.attr("disabled", false);
    });

    // Submit form to kimai
    $form.submit(function(e) {
        e.preventDefault();

        // Disable the submit button, gets enabled again on field changes
        $formSubmit.attr("disabled", true);

        // Ajax time
        $.ajax({
            data: getKimaiString(currentUserID),
            type: "POST",
            url: kimaiProcessorUrl,
            success: function() {
                // Show success message for 6 seconds
                $alertSuccess.removeClass("d-none");
                setTimeout(function() {
                    $alertSuccess.addClass("d-none");
                }, 6000);
            },
            error: function() {
                // Show error message for 6 seconds
                $alertError.removeClass("d-none");
                setTimeout(function() {
                    $alertError.addClass("d-none");
                }, 6000);
            }
        });
    });
});
